package main

import (
	"io/ioutil"

	"github.com/barx/jwt-auth"
	"gopkg.in/yaml.v2"
)

// DatabaseConfig holds information needed to connect to database
type DatabaseConfig struct {
	Server   string
	Username string
	Password string
}

// Config This structure holds all the required configuration settings for the server
type Config struct {
	Server string
	//Database            string
	Database            DatabaseConfig
	WebRoot             string
	Auth                auth.LdapProvider
	UseTLS              bool
	ServerCertificate   string
	ServerKey           string
	ReadTimeoutSeconds  int64
	WriteTimeoutSeconds int64
}

// GetDal get the Data Abstraction Layer to manage DB agnostic interactions
func (c *Config) GetDal() (Dal, error) {
	return CreateDgraphConn(c.Database.Server)
}

// Save Save the current config to disk
func (c *Config) Save() error {
	dat, err := yaml.Marshal(c)

	if err != nil {
		return err
	}

	return ioutil.WriteFile(ConfigPath, dat, 0600)
}

// Load Load config from disk
func Load() (*Config, error) {
	var dat []byte
	var err error
	config := &Config{}
	dat, err = ioutil.ReadFile(ConfigPath)
	if err != nil {
		return nil, err
	}

	err = yaml.Unmarshal(dat, config)

	if err != nil {
		return nil, err
	}
	return config, nil
}
