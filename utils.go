package main

import "net/http"

func enableCors(w http.ResponseWriter, allMethods bool) {
	(w).Header().Set("Access-Control-Allow-Origin", "*")
	if allMethods {
		(w).Header().Set("Access-Control-Allow-Methods", "POST, GET, OPTIONS, PUT, DELETE")
		(w).Header().Set("Access-Control-Allow-Headers", "Accept, Content-Type, Content-Length, Accept-Encoding, Authorization")
	}
}

func isStringInArray(s string, a []string) bool {
	for _, b := range a {
		if s == b {
			return true
		}
	}
	return false
}
