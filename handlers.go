package main

import (
	"encoding/base64"
	"encoding/json"
	"fmt"
	"net/http"
	"strings"

	aristaeus "github.com/barx/aristaeuslib"
	auth "github.com/barx/jwt-auth"
	"github.com/gorilla/context"
)

type notFoundRedirectRespWr struct {
	http.ResponseWriter
	status int
}

/* Put in a handler to wrap file serve for ember.
   ember's paths will break this, so redirect to index
   when there's a file not found
*/
func (w *notFoundRedirectRespWr) WriteHeader(status int) {
	w.status = status
	if status != http.StatusNotFound {
		w.ResponseWriter.WriteHeader(status)
	}
}

func (w *notFoundRedirectRespWr) Write(p []byte) (int, error) {
	if w.status != http.StatusNotFound {
		return w.ResponseWriter.Write(p)
	}
	return len(p), nil // fake consumed ok
}

func wrapHandler(h http.Handler) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		nfrw := &notFoundRedirectRespWr{ResponseWriter: w}
		h.ServeHTTP(nfrw, r)
		if nfrw.status == http.StatusNotFound {
			http.Redirect(w, r, "/", http.StatusFound)
		}
	}
}

func (config *Config) handleUpload(w http.ResponseWriter, r *http.Request) {
	user, _ := context.Get(r, auth.UserKey).(string)
	decoder := json.NewDecoder(r.Body)

	var data jsonAPIWrapper
	var raw []byte
	var err error
	var dal Dal
	if err = decoder.Decode(&data); err != nil {
		w.WriteHeader(http.StatusBadRequest)
		fmt.Printf("Failed to parse data: %v\n", err)
		return
	}

	hive := aristaeus.CreateHive(data.Data.Attributes.Label,
		data.Data.Attributes.Description,
		data.Data.Attributes.Classification,
		data.Data.Attributes.Hostname,
		data.Data.Attributes.OpNumber,
		user,
		data.Data.Attributes.DateAcquired,
	)

	formatHeader := "data:application/octet-stream;base64,"
	if !strings.HasPrefix(data.Data.Attributes.RawBase64string, formatHeader) {
		fmt.Printf("Failed to parse format header: %v\n", err)
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	if raw, err = base64.StdEncoding.DecodeString(strings.TrimPrefix(data.Data.Attributes.RawBase64string, formatHeader)); err != nil {
		fmt.Printf("Failed to decode data: %v\n", err)
		w.WriteHeader(http.StatusBadRequest)
		return
	}
	if err = hive.ParseFromBytes(raw); err != nil {
		fmt.Printf("Failed to parse hive: %v\n", err)
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	//_, err = UploadHiveAsync(config.Database.Server, config.Database.Username, config.Database.Password, hive)
	dal, err = config.GetDal()
	if err != nil {
		fmt.Println(err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	_, err = UploadHiveAsync(dal, hive)

	if err != nil {
		fmt.Println(err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	w.WriteHeader(http.StatusCreated)
	w.Write(([]byte)("{}"))
}
