package main

import (
	"fmt"
	"strings"
	"time"

	aristaeus "github.com/barx/aristaeuslib"
	"github.com/google/uuid"
)

const (
	// Pending job is pending
	Pending = "PENDING"
	// Running job is running
	Running = "RUNNING"
	// Complete job is complete
	Complete = "COMPLETE"
	// Error job terminated with error
	Error = "ERROR"
)

// Status the status of a submitted hive processing job
type Status struct {
	Message string `json:"message"`
	Status  string `json:"status"`
}

// Job represents a hive processing job
type Job struct {
	ID        string     `json:"id,omitempty"`
	Start     *time.Time `json:"start,omitempty"`
	Stop      *time.Time `json:"stop,omitempty"`
	CreatedBy string     `json:"created_by,omitempty"`
	Message   string     `json:"message,omitempty"`
	Status    string     `json:"status,omitempty"`
}

func utf8StringFromData(node *aristaeus.RegNode) string {
	switch node.AdataType {
	case aristaeus.RegExpandSz:
	case aristaeus.RegSz:
		return aristaeus.UnpackUtf16(node.Adata)
	case aristaeus.RegMultiSz:
		return strings.Join(aristaeus.UnpackUtf16Array(node.Adata), " ")
	}
	return ""
}

func pendingStatus(message string) Status {
	return Status{Message: message, Status: Pending}
}

func runningStatus(message string) Status {
	return Status{Message: message, Status: Running}
}

func errorStatus(message string) Status {
	return Status{Message: message, Status: Error}
}

func completeStatus(message string) Status {
	return Status{Message: message, Status: Complete}
}

// Dal Functions for storing hive data- source agnostic
type Dal interface {
	Validate() error
	Close()
	AddJob(*Job) error
	AddNode(aristaeus.RegNodePtr) (bool, error)
	AddHive(*aristaeus.Hive) error
}

// UploadHiveAsync Asynchronously upload the hive to the DB
func UploadHiveAsync(store Dal, hive *aristaeus.Hive) (chan bool, error) {
	var err error
	var exists bool

	updateProgress := make(chan Status)
	itt := aristaeus.CreateIterator(hive.Map)

	go func() {
		defer close(updateProgress)

		if err = store.AddHive(hive); err != nil {
			updateProgress <- errorStatus(fmt.Sprintf("failed to save hive: %s", err))
			return
		}

		updateProgress <- runningStatus("Storing Nodes")

		n := itt.Current()
		for itt.MoreResults() {
			r, ok := n.(aristaeus.RegNodePtr)
			if !ok {
				updateProgress <- errorStatus("bad nodeptr format")
				return
			}
			if exists, err = store.AddNode(r); err != nil {
				updateProgress <- errorStatus("failed to query datastore")
				return
			}
			if exists {
				n = itt.Next()
			} else {
				n = itt.Step()
			}
		}
	}()

	done := make(chan bool)
	go func() {
		defer close(done)
		defer store.Close()

		start := time.Now()
		job := &Job{ID: uuid.New().String(), Status: Pending, CreatedBy: hive.CreatedBy, Start: &start, Message: "Job Queued", Stop: nil}

		store.AddJob(job)

		for update := range updateProgress {
			job.Message = update.Message
			job.Status = update.Status
			store.AddJob(job)
		}
		stop := time.Now()
		job.Stop = &stop
		store.AddJob(job)
	}()
	return done, nil
}
