package main

import (
	"fmt"
	"log"
	"net/http"
	"net/http/httputil"
	"net/url"
	"os"
	"strings"
	"time"

	"github.com/gorilla/handlers"
	"github.com/gorilla/mux"
)

func main() {
	config, err := Load()

	if err != nil {
		fmt.Printf("Failed to load config file: %v\n", err)
		os.Exit(-1)
	}

	apiPrefix := "/api/v1"
	target, _ := url.Parse(config.Database.Server)

	l := NewRotateLog()
	log.SetOutput(l)

	director := func(r *http.Request) {
		r.URL.Path = strings.TrimPrefix(r.URL.Path, apiPrefix)
		r.Header.Add("X-Forwarded-Host", r.Host)
		r.Header.Add("X-Origin-Host", target.Host)
		r.URL.Scheme = "http"
		r.URL.Host = target.Host
	}

	proxy := &httputil.ReverseProxy{Director: director}

	r := mux.NewRouter()
	r.Handle("/api/token-auth/", config.Auth.GetTokenHandler()).Methods("POST", "GET")

	uploadRouter := r.PathPrefix("/upload").Subrouter()

	uploadRouter.HandleFunc("", config.handleUpload).Methods("POST")
	uploadRouter.Use(config.Auth.GetAuthMiddleware())

	apiRouter := r.PathPrefix(apiPrefix).Subrouter()
	apiRouter.PathPrefix("/").Handler(proxy)
	apiRouter.Use(config.Auth.GetAuthMiddleware())

	r.PathPrefix("/").Handler(wrapHandler(http.FileServer(http.Dir(config.WebRoot))))

	srv := &http.Server{
		Handler:      handlers.LoggingHandler(l, r),
		Addr:         config.Server,
		WriteTimeout: (time.Duration)(config.WriteTimeoutSeconds) * time.Second,
		ReadTimeout:  (time.Duration)(config.ReadTimeoutSeconds) * time.Second,
	}

	if config.UseTLS {
		if _, err := os.Stat(config.ServerCertificate); err != nil {
			fmt.Printf("Could not find server certificate file: %v\n", err)
			os.Exit(-1)
		}

		if _, err := os.Stat(config.ServerKey); err != nil {
			fmt.Printf("Could not find server key file: %v\n", err)
			os.Exit(-1)
		}
		log.Fatal(srv.ListenAndServeTLS(config.ServerCertificate, config.ServerKey))
		fmt.Println("Exiting HTTPS Server")
	} else {
		log.Fatal(srv.ListenAndServe())
		fmt.Println("Exiting HTTP Server")
	}

}
