package main

import "time"

type uploadedData struct {
	Hostname        string    `json:"hostname"`
	OpNumber        string    `json:"op_number"`
	Classification  string    `json:"classification"`
	Label           string    `json:"label"`
	Description     string    `json:"description"`
	DateAcquired    time.Time `json:"date_acquired"`
	RawBase64string string    `json:"raw"`
}

type jsonAPIData struct {
	Attributes uploadedData `json:"attributes"`
}

type jsonAPIWrapper struct {
	Data jsonAPIData `json:"data"`
}
