package main

import (
	"errors"
	"fmt"
	"strings"
	"time"

	driver "github.com/arangodb/go-driver"
	"github.com/arangodb/go-driver/http"
	aristaeus "github.com/barx/aristaeuslib"
)

const (
	rootNodeLabel = "__ROOT_NODE__"
)

// ArangoJob Structure for storing data on submitted jobs
type ArangoJob struct {
	Key       string     `json:"_key,omitempty"`
	ID        string     `json:"_id,omitempty"`
	Rev       string     `json:"_rev,omitempty"`
	Start     *time.Time `json:"start,omitempty"`
	Stop      *time.Time `json:"stop,omitempty"`
	CreatedBy string     `json:"created_by,omitempty"`
	Message   string     `json:"message,omitempty"`
	Status    string     `json:"status,omitempty"`
}

// ArangoEdge is the basic edge type for arangodb
type ArangoEdge struct {
	Key  string `json:"_key,omitempty"`
	From string `json:"_from"`
	To   string `json:"_to"`
}

// ArangoHive structure for storing hive in ArangoDB
type ArangoHive struct {
	Key            string    `json:"_key,omitempty"`
	ID             string    `json:"_id,omitempty"`
	Description    string    `json:"description,omitempty"`
	Label          string    `json:"label,omitempty"`
	Hostname       string    `json:"hostname,omitempty"`
	Classification string    `json:"classification,omitempty"`
	OpNumber       string    `json:"op_number,omitempty"`
	DateAcquired   time.Time `json:"date_acquired,omitempty"`

	Created   time.Time `json:"created,omitempty"`
	CreatedBy string    `json:"created_by,omitempty"`
	Rev       string    `json:"_rev,omitempty"`
}

// ArangoKey structure for storing keys in ArangoDB
type ArangoKey struct {
	ID         string     `json:"_id,omitempty"`
	Key        string     `json:"_key,omitempty"`
	ClassName  string     `json:"class_name,omitempty"`
	Hash       string     `json:"hash,omitempty"`
	Path       string     `json:"path,omitempty"`
	Name       string     `json:"name,omitempty"`
	Timestamp  *time.Time `json:"timestamp,omitempty"`
	SearchText string     `json:"search_text,omitempty"`
}

// ArangoValue structure for storing values in ArangoDB
type ArangoValue struct {
	ID         string `json:"_id,omitempty"`
	Key        string `json:"_key,omitempty"`
	DataSize   uint16 `json:"data_size,omitempty"`
	DataType   uint16 `json:"data_type,omitempty"`
	Hash       string `json:"hash,omitempty"`
	DataHash   string `json:"data_hash,omitempty"`
	Path       string `json:"path,omitempty"`
	Name       string `json:"name,omitempty"`
	SearchText string `json:"search_text,omitempty"`
}

// ArangoData structure for storing value data in ArangoDB
type ArangoData struct {
	ID         string `json:"_id,omitempty"`
	Key        string `json:"_key,omitempty"`
	Data       []byte `json:"data,omitempty"`
	SearchText string `json:"search_text,omitempty"`
}

type arangoItem interface {
	getKey() string
}

func (ae ArangoEdge) getKey() string {
	return ae.Key
}

func (aj ArangoJob) getKey() string {
	return aj.Key
}

func (ah ArangoHive) getKey() string {
	return ah.Key
}

func (ak ArangoKey) getKey() string {
	return ak.Key
}

func (av ArangoValue) getKey() string {
	return av.Key
}

func (ad ArangoData) getKey() string {
	return ad.Key
}

func (ah *ArangoHive) fromHive(hive *aristaeus.Hive) {
	ah.Description = hive.Description
	ah.Label = hive.Label
	ah.Classification = hive.Classification
	ah.Hostname = hive.Hostname
	ah.OpNumber = hive.OpNumber
	ah.DateAcquired = hive.DateAcquired
	ah.Created = hive.Created
	ah.CreatedBy = hive.CreatedBy
}

func toArangoKey(node *aristaeus.RegNode) ArangoKey {
	return ArangoKey{
		ClassName: node.AclassName,
		Hash:      node.Ahash,
		Path:      node.Apath,
		Name:      node.Aname,
		Timestamp: node.Atimestamp,
		Key:       node.Ahash,
		//SearchText: strings.ToLower(fmt.Sprintf("%s %s", getNameFromPath(node.Apath), node.AclassName)),
		SearchText: strings.ToLower(fmt.Sprintf("%s %s", node.Aname, node.AclassName)),
	}
}

func toArangoValue(node *aristaeus.RegNode) ArangoValue {
	return ArangoValue{
		DataSize:   node.AdataSize,
		DataType:   node.AdataType,
		Hash:       node.Ahash,
		DataHash:   node.Adatahash,
		Name:       node.Aname,
		Path:       node.Apath,
		Key:        node.Ahash,
		SearchText: strings.ToLower(node.Aname),
	}
}

func toArangoData(node *aristaeus.RegNode) ArangoData {
	return ArangoData{
		Data:       node.Adata,
		Key:        node.Adatahash,
		SearchText: strings.ToLower(utf8StringFromData(node)),
	}
}

func (aj *ArangoJob) fromJob(j *Job) {
	aj.Key = j.ID
	aj.Start = j.Start
	aj.Stop = j.Stop
	aj.CreatedBy = j.CreatedBy
	aj.Message = j.Message
	aj.Status = j.Status
}

// ArangoConn struct for DGraph connectivity
type ArangoConn struct {
	//client     driver.Client
	servername string
	//conn       driver.Connection
	db driver.Database

	edges map[string]*ArangoEdge
}

// Close closes the connection
func (ac ArangoConn) Close() {
	//ac.conn.Close()
}

// CreateArangoConn creates connection to DGraph server
func CreateArangoConn(server, username, password string) (ArangoConn, error) {
	var err error
	var conn driver.Connection
	var c driver.Client
	client := ArangoConn{servername: server}
	client.edges = make(map[string]*ArangoEdge)

	if conn, err = http.NewConnection(http.ConnectionConfig{
		Endpoints: []string{server},
	}); err != nil {
		return client, err
	}

	if c, err = driver.NewClient(driver.ClientConfig{
		Connection: conn,
	}); err != nil {
		return client, err
	}

	client.db, err = c.Database(nil, "aristaeus")

	return client, err
}

func (ac ArangoConn) upsert(obj arangoItem, colName string) (bool, string, error) {
	var err error
	var exists bool
	var col driver.Collection
	var meta driver.DocumentMeta
	if col, err = ac.db.Collection(nil, colName); err != nil {
		return false, "", err
	}

	if exists, err = col.DocumentExists(nil, obj.getKey()); err != nil {
		return false, "", err
	}

	if exists {
		return true, "", nil
	}

	meta, err = col.CreateDocument(nil, obj)
	return false, meta.ID.String(), err
}

// AddHive stores the hive node in the database
func (ac ArangoConn) AddHive(hive *aristaeus.Hive) error {
	hives, err := ac.db.Collection(nil, "hives")
	if err != nil {
		return err
	}
	obj := ArangoHive{}
	obj.fromHive(hive)

	meta, err := hives.CreateDocument(nil, obj)

	ac.edges[rootNodeLabel] = &ArangoEdge{From: meta.ID.String()}
	return err
}

func (ac ArangoConn) addEdge(key, path, edgeType string, deleteAfter bool) error {
	if edge, ok := ac.edges[path]; ok {
		if deleteAfter {
			delete(ac.edges, path)
		}
		edge.To = key
		_, _, err := ac.upsert(edge, edgeType)
		return err
	}
	return nil
}

// AddNode adds a node to the data store
func (ac ArangoConn) AddNode(np aristaeus.RegNodePtr) (bool, error) {
	var err error
	var exists bool
	var itemID string
	switch np.NodeType() {
	case "value":
		if exists, itemID, err = ac.upsert(toArangoValue(np.Value), "values"); err != nil {
			return exists, err
		}

		if !exists {
			if err = ac.addEdge(itemID, np.Path(), "values_of", true); err != nil {
				return exists, err
			}

			_, _, err = ac.upsert(toArangoData(np.Value), "data")
		}

		return exists, err
	case "key":
		colName := "keys"

		if exists, itemID, err = ac.upsert(toArangoKey(np.Value), colName); err != nil {

			// if the key doesn't exist, queue it's child edges to be added
			// with arango, the linked node needs to exist before we add the child
			// so we will add the edge when the child is processed
			if !exists {
				for _, childPath := range np.Children() {
					ac.edges[childPath] = &ArangoEdge{From: itemID}
				}

				var rel, path string
				var del bool

				// also, add the edge from the current keys parent
				if strings.Count(np.Path(), "/") == 1 {
					// root node
					path = rootNodeLabel
					rel = "roots"
					del = false
				} else {
					path = np.Path()
					rel = "keys_of"
					del = true
				}

				err = ac.addEdge(itemID, path, rel, del)
			}
		}
	default:
		return false, errors.New("bad node type")
	}
	return exists, err
}

/*
func uploadKey(col driver.Collection, key ArangoKey) (id string, exists bool, err error) {
	var meta driver.DocumentMeta
	//var exists bool
	exists = false

	if exists, err = col.DocumentExists(nil, key.Hash); err != nil {
		id = ""
		return
	}

	if exists {
		existing := &ArangoKey{}
		meta, err = col.ReadDocument(nil, key.Hash, existing)
	} else {
		meta, err = col.CreateDocument(nil, key)
	}

	if err != nil {
		id = ""
	} else {
		id = string(meta.ID)
	}
	return
}

func uploadData(col driver.Collection, data ArangoData) (err error) {
	var exists bool

	if exists, err = col.DocumentExists(nil, data.Key); err != nil {
		return
	}

	if !exists {
		_, err = col.CreateDocument(nil, data)
	}

	return
}

func uploadValue(col driver.Collection, val ArangoValue) (id string, exists bool, err error) {
	var meta driver.DocumentMeta
	//var exists bool
	exists = false

	if exists, err = col.DocumentExists(nil, val.Hash); err != nil {
		id = ""
		return
	}

	if exists {
		existing := &ArangoKey{}
		meta, err = col.ReadDocument(nil, val.Hash, existing)
	} else {
		meta, err = col.CreateDocument(nil, val)
	}

	if err != nil {
		id = ""
	} else {
		id = string(meta.ID)
	}
	return
}

type ArangoBatchClient struct {
	//"http://localhost:8529/_api/import?type=documents&collection=test"
	Server   string
	Username string
	Password string
	db       driver.Database
}

func (abc ArangoBatchClient) bulkUpdate(collection string) {

}

// Validate validates the ArangoConnection and DB
func (abc ArangoBatchClient) Validate() error {
	var err error
	var conn driver.Connection
	var c driver.Client

	collections := []string{"jobs", "hives", "keys", "values", "data", "v", "keys_of", "values_of"}
	if conn, err = ahttp.NewConnection(ahttp.ConnectionConfig{
		Endpoints: []string{abc.Server},
	}); err != nil {
		return fmt.Errorf("could not connect to database: %s", err)
	}

	if c, err = driver.NewClient(driver.ClientConfig{
		Connection:     conn,
		Authentication: driver.BasicAuthentication(abc.Username, abc.Password),
	}); err != nil {
		return fmt.Errorf("could not connect to database: %s", err)
	}

	if abc.db, err = c.Database(nil, "aristaeus"); err != nil {
		return fmt.Errorf("could not connect to database: %s", err)
	}

	for _, collection := range collections {
		if _, err = abc.db.Collection(nil, collection); err != nil {
			return fmt.Errorf("database not initialized: %s", err)
		}
	}
	return nil
}

// UploadHiveAsync object to arangoDB
func UploadHiveAsync(server, username, password string, hive *aristaeus.Hive) (chan bool, error) {
	//var conn driver.Connection
	//var c driver.Client
	//var hives, keys, values, rootnodes, keysOf, valuesOf, data, jobs driver.Collection
	//var db driver.Database
	var err error

	var dbClient = &http.Client{
		Timeout: time.Second * 10,
	}

	response, _ := dbClient.Post(url, "application/json")
	if response.StatusCode != http.StatusOK {

	}

	//func (c *Client) Post(url, contentType string, body io.Reader) (resp *Response, err error)

	updateProgress := make(chan Status)

	keyCount := 0
	valueCount := 0

	go func() {
		defer close(updateProgress)

		obj := ArangoHive{}
		obj.fromHive(hive)

		var meta driver.DocumentMeta

		if meta, err = hives.CreateDocument(nil, obj); err != nil {
			// Handle error
			updateProgress <- errorStatus(fmt.Sprintf("failed to create hive document: %s", err))
			return
		}
		hiveID := string(meta.ID)

		keyLookup := make(map[string]string)
		valueLookup := make(map[string]string)

		valuesToLoad := make([]ArangoValue, 0, 1000)
		dataToLoad := make([]ArangoData, 0, 1000)
		keysToLoad := make([]ArangoKey, 0, 1000)
		//valuesToLoad = append(valuesToLoad, nil)

		updateProgress <- runningStatus("Storing Nodes")
		// First pass, store nodes

		srcId := "hive"      // update this to hive id
		colPtr := "rootNode" // update this to rootnode collection
		for _, v := range hive.Map {
			var id string
			var exists bool
			switch v.NodeType() {
			case "value":
				valuesToLoad = append(valuesToLoad, toArangoValue(v.Value))
				dataToLoad = append(dataToLoad, toArangoData(v.Value))
				break
			case "key":
				k := toArangoKey(v.Value)
				keysToLoad = append(keysToLoad, k)
				k.Key // prepend with collectionname

				for _, childPath := range v.Children() {
					childPtr, ok := hive.Map[childPath]
					// switch child node to to get correct collection
					//edge := ArangoEdge{From: , To: }
				}

				break
			default:
				updateProgress <- errorStatus(fmt.Sprintf("bad node type: %s", v.NodeType()))
				return
			}
		}

		// POST data
		// POST values
		// POST keys

		updateProgress <- runningStatus("Storing Edges")

		edgeCount := 0
		for path, iD := range keyLookup {
			nPtr, ok := hive.Map[path]
			if !ok {
				//panic("Failed to look up path from hive map!")
				updateProgress <- errorStatus("Failed to look up path from hive map!")
				return
			}

			// Create root node links if applicable
			depth := strings.Count(nPtr.Path(), "/")
			if depth == 1 {
				edge := ArangoEdge{From: hiveID, To: iD}

				if meta, err = rootnodes.CreateDocument(nil, edge); err != nil {
					// Handle error
					//unique constraint violated
					//if !driver.IsConflict(err) {
					//fmt.Printf("Failed creating edge from %s to %s\n", edge.From, edge.To)
					//return err

					updateProgress <- errorStatus(fmt.Sprintf("Failed creating edge from %s to %s\n", edge.From, edge.To))
					return
					//}
				}
				edgeCount++
			}

			for _, childPath := range nPtr.Value.Achildren {

				childPtr, ok := hive.Map[childPath]
				if !ok {
					updateProgress <- errorStatus("Failed to look up path!")
					return
				}
				childNode := childPtr.Value
				switch childNode.Atype {
				case "key":
					childID, ok := keyLookup[childNode.Apath]
					if !ok {
						//panic("Failed to look up path!")
						continue
					}
					edge := ArangoEdge{From: iD, To: childID}

					if meta, err = keysOf.CreateDocument(nil, edge); err != nil {
						// Handle error
						if !driver.IsConflict(err) {
							updateProgress <- errorStatus(fmt.Sprintf("Failed creating edge from %s to %s\n", edge.From, edge.To))
							return
						}
					} else {
						edgeCount++
					}

					break
				case "value":
					childID, ok := valueLookup[childNode.Apath]
					if !ok {
						updateProgress <- errorStatus("Failed to look up value!")
						return
					}
					edge := ArangoEdge{From: iD, To: childID}

					if meta, err = valuesOf.CreateDocument(nil, edge); err != nil {

						if !driver.IsConflict(err) {
							updateProgress <- errorStatus(fmt.Sprintf("Failed creating edge from %s to %s\n", edge.From, edge.To))
							return
						}
					} else {
						edgeCount++
					}

					break
				default:
					updateProgress <- errorStatus("Bad node type")
					return
				}

			}
		}
		updateProgress <- completeStatus("Successfully stored hive")
	}()
	start := time.Now()
	job := ArangoJob{Status: Pending, CreatedBy: hive.CreatedBy, Start: &start, Message: "Job Queued", Stop: nil}

	var meta driver.DocumentMeta
	if meta, err = jobs.CreateDocument(nil, job); err != nil {
		return nil, err
	}
	done := make(chan bool)
	go func() {
		defer close(done)
		for update := range updateProgress {
			job.Message = update.Message
			job.Status = update.Status
			jobs.ReplaceDocument(nil, meta.Key, job)
		}
		stop := time.Now()
		job.Stop = &stop
		jobs.ReplaceDocument(nil, meta.Key, job)
	}()

	return done, nil
}

// UploadHiveAsync object to arangoDB
/*
func UploadHiveAsync(server, username, password string, hive *aristaeus.Hive) (chan bool, error) {
	var conn driver.Connection
	var c driver.Client
	var hives, keys, values, rootnodes, keysOf, valuesOf, data, jobs driver.Collection
	var db driver.Database
	var err error

	if conn, err = http.NewConnection(http.ConnectionConfig{
		Endpoints: []string{server},
	}); err != nil {
		return nil, fmt.Errorf("could not connect to database: %s", err)
	}

	if c, err = driver.NewClient(driver.ClientConfig{
		Connection:     conn,
		Authentication: driver.BasicAuthentication(username, password),
	}); err != nil {
		return nil, fmt.Errorf("could not connect to database: %s", err)
	}

	if db, err = c.Database(nil, "aristaeus"); err != nil {
		return nil, fmt.Errorf("could not connect to database: %s", err)
	}

	if jobs, err = db.Collection(nil, "jobs"); err != nil {
		return nil, fmt.Errorf("database not initialized: %s", err)
	}

	updateProgress := make(chan Status)

	keyCount := 0
	valueCount := 0

	go func() {
		defer close(updateProgress)

		if hives, err = db.Collection(nil, "hives"); err != nil {
			updateProgress <- errorStatus(fmt.Sprintf("database not initialized: %s", err))
			return
		}

		if keys, err = db.Collection(nil, "keys"); err != nil {
			updateProgress <- errorStatus(fmt.Sprintf("database not initialized: %s", err))
			return
		}

		if values, err = db.Collection(nil, "values"); err != nil {
			updateProgress <- errorStatus(fmt.Sprintf("database not initialized: %s", err))
			return
		}

		if data, err = db.Collection(nil, "data"); err != nil {
			updateProgress <- errorStatus(fmt.Sprintf("database not initialized: %s", err))
			return
		}

		if rootnodes, err = db.Collection(nil, "roots"); err != nil {
			updateProgress <- errorStatus(fmt.Sprintf("database not initialized: %s", err))
			return
		}
		if keysOf, err = db.Collection(nil, "keys_of"); err != nil {
			updateProgress <- errorStatus(fmt.Sprintf("database not initialized: %s", err))
			return
		}
		if valuesOf, err = db.Collection(nil, "values_of"); err != nil {
			updateProgress <- errorStatus(fmt.Sprintf("database not initialized: %s", err))
			return
		}

		obj := ArangoHive{}
		obj.fromHive(hive)

		var meta driver.DocumentMeta

		if meta, err = hives.CreateDocument(nil, obj); err != nil {
			// Handle error
			updateProgress <- errorStatus(fmt.Sprintf("failed to create hive document: %s", err))
			return
		}
		hiveID := string(meta.ID)

		keyLookup := make(map[string]string)
		valueLookup := make(map[string]string)

		updateProgress <- runningStatus("Storing Nodes")
		// First pass, store nodes
		for _, v := range hive.Map {
			var id string
			var exists bool
			switch v.NodeType() {
			case "value":
				val := toArangoValue(v.Value)
				valueData := toArangoData(v.Value)
				// Store data value separately...
				if err := uploadData(data, valueData); err != nil {
					//return fmt.Errorf("failed to save data: %s", err)
					updateProgress <- errorStatus(fmt.Sprintf("failed to save data: %s", err))
					return
				}

				if id, exists, err = uploadValue(values, val); err != nil {
					updateProgress <- errorStatus(fmt.Sprintf("failed to save value: %s", val.Path))
					return
				}
				valueLookup[val.Path] = id
				if !exists {
					valueCount++
				}
				break
			case "key":
				key := toArangoKey(v.Value)

				if id, exists, err = uploadKey(keys, key); err != nil {
					updateProgress <- errorStatus(fmt.Sprintf("failed to save key: %s", key.Path))
					return
				}
				keyLookup[key.Path] = id
				if !exists {
					keyCount++
				}

				break
			default:
				updateProgress <- errorStatus(fmt.Sprintf("bad node type: %s", v.NodeType()))
				return
			}
		}

		updateProgress <- runningStatus("Storing Edges")

		edgeCount := 0
		for path, iD := range keyLookup {
			nPtr, ok := hive.Map[path]
			if !ok {
				//panic("Failed to look up path from hive map!")
				updateProgress <- errorStatus("Failed to look up path from hive map!")
				return
			}

			// Create root node links if applicable
			depth := strings.Count(nPtr.Path(), "/")
			if depth == 1 {
				edge := ArangoEdge{From: hiveID, To: iD}

				if meta, err = rootnodes.CreateDocument(nil, edge); err != nil {
					// Handle error
					//unique constraint violated
					//if !driver.IsConflict(err) {
					//fmt.Printf("Failed creating edge from %s to %s\n", edge.From, edge.To)
					//return err

					updateProgress <- errorStatus(fmt.Sprintf("Failed creating edge from %s to %s\n", edge.From, edge.To))
					return
					//}
				}
				edgeCount++
			}

			for _, childPath := range nPtr.Value.Achildren {

				childPtr, ok := hive.Map[childPath]
				if !ok {
					updateProgress <- errorStatus("Failed to look up path!")
					return
				}
				childNode := childPtr.Value
				switch childNode.Atype {
				case "key":
					childID, ok := keyLookup[childNode.Apath]
					if !ok {
						//panic("Failed to look up path!")
						continue
					}
					edge := ArangoEdge{From: iD, To: childID}

					if meta, err = keysOf.CreateDocument(nil, edge); err != nil {
						// Handle error
						if !driver.IsConflict(err) {
							updateProgress <- errorStatus(fmt.Sprintf("Failed creating edge from %s to %s\n", edge.From, edge.To))
							return
						}
					} else {
						edgeCount++
					}

					break
				case "value":
					childID, ok := valueLookup[childNode.Apath]
					if !ok {
						updateProgress <- errorStatus("Failed to look up value!")
						return
					}
					edge := ArangoEdge{From: iD, To: childID}

					if meta, err = valuesOf.CreateDocument(nil, edge); err != nil {

						if !driver.IsConflict(err) {
							updateProgress <- errorStatus(fmt.Sprintf("Failed creating edge from %s to %s\n", edge.From, edge.To))
							return
						}
					} else {
						edgeCount++
					}

					break
				default:
					updateProgress <- errorStatus("Bad node type")
					return
				}

			}
		}
		updateProgress <- completeStatus("Successfully stored hive")
	}()
	start := time.Now()
	job := ArangoJob{Status: Pending, CreatedBy: hive.CreatedBy, Start: &start, Message: "Job Queued", Stop: nil}

	var meta driver.DocumentMeta
	if meta, err = jobs.CreateDocument(nil, job); err != nil {
		return nil, err
	}
	done := make(chan bool)
	go func() {
		defer close(done)
		for update := range updateProgress {
			job.Message = update.Message
			job.Status = update.Status
			jobs.ReplaceDocument(nil, meta.Key, job)
		}
		stop := time.Now()
		job.Stop = &stop
		jobs.ReplaceDocument(nil, meta.Key, job)
	}()

	return done, nil
}
*/
