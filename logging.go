package main

import (
	"os"
	"path/filepath"
	"sync"
	"time"
)

const (
	intervalPeriod time.Duration = 24 * time.Hour
	hourToTick     int           = 0
	minuteToTick   int           = 0
	secondToTick   int           = 0
)

type jobTicker struct {
	t *time.Timer
}

func getNextTickDuration() time.Duration {
	now := time.Now()
	nextTick := time.Date(now.Year(), now.Month(), now.Day(), hourToTick, minuteToTick, secondToTick, 0, time.Local)
	if nextTick.Before(now) {
		nextTick = nextTick.Add(intervalPeriod)
	}
	return nextTick.Sub(time.Now())
}

func newJobTicker() jobTicker {
	return jobTicker{time.NewTimer(getNextTickDuration())}
}

func (jt jobTicker) updateJobTicker() {
	jt.t.Reset(getNextTickDuration())
}

func (w *RotateWriter) runRotateLogFile() {
	jt := newJobTicker()
	for {
		<-jt.t.C
		w.rotate()
		jt.updateJobTicker()
	}
}

// RotateWriter rotating log file
type RotateWriter struct {
	lock     sync.Mutex
	filename string // should be set to the actual filename
	fp       *os.File
}

func newTimeBasedLogFileName() string {
	n := time.Now()
	//2006-01-02
	f := n.Format("20060102.log")
	return filepath.Join(LogRoot, f)
}

// NewRotateLog Make a new RotateWriter. Return nil if error occurs during setup.
func NewRotateLog() *RotateWriter {
	w := &RotateWriter{}
	err := w.rotate()
	if err != nil {
		return nil
	}
	go w.runRotateLogFile()
	return w
}

// Write satisfies the io.Writer interface.
func (w *RotateWriter) Write(output []byte) (int, error) {
	w.lock.Lock()
	defer w.lock.Unlock()
	return w.fp.Write(output)
}

// Perform the actual act of rotating and reopening file.
func (w *RotateWriter) rotate() (err error) {
	w.lock.Lock()
	defer w.lock.Unlock()

	// Close existing file if open
	if w.fp != nil {
		w.fp.Sync()
		err = w.fp.Close()
		w.fp = nil
		if err != nil {
			return
		}
	}
	w.filename = newTimeBasedLogFileName()
	w.fp, err = os.OpenFile(w.filename, os.O_RDWR|os.O_CREATE|os.O_APPEND, 0660)
	return
}
