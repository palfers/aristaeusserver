package main

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"strings"
	"time"

	aristaeus "github.com/barx/aristaeuslib"
	"github.com/dgraph-io/dgo"
	"github.com/dgraph-io/dgo/protos/api"
	"google.golang.org/grpc"
)

/*
type dGraphItem interface {
	getUID() string
}

func (dj DGraphJob) getUID() string {
	return dj.UID
}

func (dh DGraphHive) getUID() string {
	return dh.UID
}

func (dk DGraphKey) getUID() string {
	return dk.UID
}

func (dv DGraphValue) getUID() string {
	return dv.UID
}

func (dd DGraphData) getUID() string {
	return dd.UID
}*/

type dGraphResponseData struct {
	Code    string            `json:"code,omitempty"`
	Message string            `json:"message,omitempty"`
	Uids    map[string]string `json:"uids,omitempty"`
}

type dGraphResponse struct {
	Data dGraphResponseData `json:"data,omitempty"`
	// Extensions
}

// DGraphJob Structure for storing data on submitted jobs
type DGraphJob struct {
	UID       string     `json:"uid,omitempty"`
	Start     *time.Time `json:"start,omitempty"`
	Stop      *time.Time `json:"stop,omitempty"`
	CreatedBy string     `json:"created_by,omitempty"`
	Message   string     `json:"message,omitempty"`
	Status    string     `json:"status,omitempty"`
}

func (dj *DGraphJob) fromJob(j *Job) {
	dj.UID = j.ID
	dj.Start = j.Start
	dj.Stop = j.Stop
	dj.CreatedBy = j.CreatedBy
	dj.Message = j.Message
	dj.Status = j.Status
}

// DGraphHive structure for storing hive in DGraph
type DGraphHive struct {
	UID            string    `json:"uid,omitempty"`
	Description    string    `json:"description,omitempty"`
	Label          string    `json:"label,omitempty"`
	Hostname       string    `json:"hostname,omitempty"`
	Classification string    `json:"classification,omitempty"`
	OpNumber       string    `json:"op_number,omitempty"`
	DateAcquired   time.Time `json:"date_acquired,omitempty"`

	Created   time.Time `json:"created,omitempty"`
	CreatedBy string    `json:"created_by,omitempty"`
}

// DGraphKey structure for storing keys in DGraph
type DGraphKey struct {
	UID        string     `json:"uid,omitempty"`
	ClassName  string     `json:"class_name,omitempty"`
	Hash       string     `json:"hash,omitempty"`
	Path       string     `json:"path,omitempty"`
	Name       string     `json:"name,omitempty"`
	Timestamp  *time.Time `json:"timestamp,omitempty"`
	SearchText string     `json:"search_text,omitempty"`
}

// DGraphValue structure for storing values in DGraph
type DGraphValue struct {
	UID        string `json:"uid,omitempty"`
	DataSize   uint16 `json:"data_size,omitempty"`
	DataType   uint16 `json:"data_type,omitempty"`
	Hash       string `json:"hash,omitempty"`
	DataHash   string `json:"data_hash,omitempty"`
	Path       string `json:"path,omitempty"`
	Name       string `json:"name,omitempty"`
	SearchText string `json:"search_text,omitempty"`
}

// DGraphData structure for storing value data in DGraph
type DGraphData struct {
	UID        string `json:"uid,omitempty"`
	Data       []byte `json:"data,omitempty"`
	SearchText string `json:"search_text,omitempty"`
}

func (dh *DGraphHive) fromHive(hive *aristaeus.Hive) {
	dh.Description = hive.Description
	dh.Label = hive.Label
	dh.Classification = hive.Classification
	dh.Hostname = hive.Hostname
	dh.OpNumber = hive.OpNumber
	dh.DateAcquired = hive.DateAcquired
	dh.Created = hive.Created
	dh.CreatedBy = hive.CreatedBy

}

func toDGraphKey(node *aristaeus.RegNode) DGraphKey {
	return DGraphKey{
		ClassName: node.AclassName,
		Hash:      node.Ahash,
		Path:      node.Apath,
		Name:      node.Aname,
		Timestamp: node.Atimestamp,
		UID:       node.Ahash,
		//SearchText: strings.ToLower(fmt.Sprintf("%s %s", getNameFromPath(node.Apath), node.AclassName)),
		SearchText: strings.ToLower(fmt.Sprintf("%s %s", node.Aname, node.AclassName)),
	}
}

func toDGraphValue(node *aristaeus.RegNode) DGraphValue {
	return DGraphValue{
		DataSize:   node.AdataSize,
		DataType:   node.AdataType,
		Hash:       node.Ahash,
		DataHash:   node.Adatahash,
		Name:       node.Aname,
		Path:       node.Apath,
		UID:        node.Ahash,
		SearchText: strings.ToLower(node.Aname),
	}
}

func toDGraphData(node *aristaeus.RegNode) DGraphData {
	return DGraphData{
		Data:       node.Adata,
		UID:        node.Adatahash,
		SearchText: strings.ToLower(utf8StringFromData(node)),
	}
}

// DGraphConn struct for DGraph connectivity
type DGraphConn struct {
	client     *dgo.Dgraph
	servername string
	conn       *grpc.ClientConn
}

// Close closes the connection
func (dgc DGraphConn) Close() {
	dgc.conn.Close()
}

// CreateDgraphConn creates connection to DGraph server
func CreateDgraphConn(server string) (DGraphConn, error) {
	var err error

	client := DGraphConn{servername: server}
	client.conn, err = grpc.Dial(server, grpc.WithInsecure())
	if err != nil {
		return client, err
	}

	client.client = dgo.NewDgraphClient(api.NewDgraphClient(client.conn))

	return client, nil
}

// Validate validates the ArangoConnection and DB
func (dgc DGraphConn) Validate() error {
	//var err error

	return nil
}

/*
func upsert(txn *dgo.Txn, obj dGraphItem) (bool, error) {
	created := false
	ctx := context.Background()
	jobj, err := json.Marshal(obj)
	if err != nil {
		return created, err
	}

	query := fmt.Sprintf(`
		query {
			item as var(func: uid(%s))
		}`, obj.getUID())
	mu := &api.Mutation{
		Cond:    `@if(eq(len(item), 0))`, // Only mutate if not present
		SetJson: jobj,
	}
	req := &api.Request{
		Query:     query,
		Mutations: []*api.Mutation{mu},
	}

	//assigned, err := dg.NewTxn().Do(ctx, mu)
	//resp, err = txn.Do(ctx, req)

	return created, err
}
*/
func (dgc DGraphConn) upsert(obj interface{}) (bool, error) {
	var err error
	var jobj []byte
	var resp *api.Response

	ctx := context.Background()
	if jobj, err = json.Marshal(obj); err != nil {
		return false, err
	}

	resp, err = dgc.client.NewTxn().Mutate(ctx, &api.Mutation{SetJson: jobj})

	respObj := &dGraphResponse{}

	if err = json.Unmarshal(resp.GetJson(), respObj); err != nil {
		return false, err
	}

	return len(respObj.Data.Uids) == 1, err
}

/*
func replace(txn *dgo.Txn, obj dGraphItem) error {
	ctx := context.Background()
	jobj, err := json.Marshal(obj)
	if err != nil {
		return err
	}

	mu := &api.Mutation{SetJson: jobj}
	req := &api.Request{
		Mutations: []*api.Mutation{mu},
		CommitNow: true,
	}

	_, err = txn.Do(ctx, req)

	return err
}*/

/*
func bulkUpsert(txn int, objs []dGraphItem) error {
	ctx := context.Background()

	mutations := make([]*api.Mutation, len(objs))

	for i := 0; i < len(objs); i++ {
		jobj, err := json.Marshal(objs[i])
		if err != nil {
			return err
		}
		query := fmt.Sprintf(`
		query {
			item as var(func: uid(%s))
		}`, objs[i].getUID())
		mutations[i] = &api.Mutation{
			Cond:    `@if(eq(len(item), 0))`, // Only mutate if not present
			SetJson: jobj,
		}
	}

	req := &api.Request{
		Query:     query,
		Mutations: mutations,
		CommitNow: true,
	}

	if _, err := txn.Mutate(ctx, req); err != nil {
		log.Fatal(err)
	}
	return err
}
*/

// AddHive stores the hive node in the database
func (dgc DGraphConn) AddHive(hive *aristaeus.Hive) error {
	obj := DGraphHive{}
	obj.fromHive(hive)
	_, err := dgc.upsert(obj)
	return err
}

// AddNode adds a node to the datastore
func (dgc DGraphConn) AddNode(np aristaeus.RegNodePtr) (bool, error) {
	var err error
	var exists bool
	switch np.NodeType() {
	case "value":
		if exists, err = dgc.upsert(toDGraphValue(np.Value)); err != nil {
			return exists, err
		}

		_, err = dgc.upsert(toDGraphData(np.Value))

		return exists, err
	case "key":
		return dgc.upsert(toDGraphKey(np.Value))
	default:
		return false, errors.New("bad node type")
	}
	//return false, nil
}

// AddJob adds a node to the datastore
func (dgc DGraphConn) AddJob(job *Job) error {
	dj := &DGraphJob{}
	dj.fromJob(job)

	_, err := dgc.upsert(dj)
	return err
}

/*
// UploadHiveAsync object to DGraph
func (dgc DGraphConn) UploadHiveAsync(hive *aristaeus.Hive) (chan bool, error) {
	var err error

	updateProgress := make(chan Status)

	go func() {
		defer close(updateProgress)
		txn := dgc.client.NewTxn()
		defer txn.Discard(context.Background())

		obj := DGraphHive{}
		obj.fromHive(hive)

		if err = dgc.insert(obj); err != nil {
			updateProgress <- errorStatus(fmt.Sprintf("failed to save hive: %s", err))
			return
		}

		updateProgress <- runningStatus("Storing Nodes")

		for _, v := range hive.Map {
			switch v.NodeType() {
			case "value":
				if err := upsert(txn, toDGraphValue(v.Value)); err != nil {
					updateProgress <- errorStatus(fmt.Sprintf("failed to save value: %s", err))
					return
				}

				if err := upsert(txn, toDGraphData(v.Value)); err != nil {
					updateProgress <- errorStatus(fmt.Sprintf("failed to save data: %s", err))
					return
				}
			case "key":
				if err := upsert(txn, toDGraphKey(v.Value)); err != nil {
					updateProgress <- errorStatus(fmt.Sprintf("failed to save data: %s", err))
					return
				}

			default:
				updateProgress <- errorStatus(fmt.Sprintf("bad node type: %s", v.NodeType()))
				return
			}
		}
	}()

	done := make(chan bool)
	go func() {
		defer close(done)
		txn := dgc.client.NewTxn()
		defer txn.Discard(context.Background())

		start := time.Now()
		job := DGraphJob{UID: uuid.New().String(), Status: Pending, CreatedBy: hive.CreatedBy, Start: &start, Message: "Job Queued", Stop: nil}

		if err := upsert(txn, job); err != nil {
			return
		}

		for update := range updateProgress {
			job.Message = update.Message
			job.Status = update.Status
			replace(txn, job)
			//jobs.ReplaceDocument(nil, meta.Key, job)
		}
		stop := time.Now()
		job.Stop = &stop
		replace(txn, job)
		//jobs.ReplaceDocument(nil, meta.Key, job)
	}()

	return done, nil
}
*/
